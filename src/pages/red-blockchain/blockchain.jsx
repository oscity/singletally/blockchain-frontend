import React, { Component } from 'react';
import Radio from '@material-ui/core/Radio';
import FormControl from '@material-ui/core/FormControl';
import NavigateBeforeIcon from '@material-ui/icons/NavigateBefore';
import TextField from '@material-ui/core/TextField';

import './blockchain.scss';
import Ethereum from '../../assets/img/eth-logo.svg';
import Bitcoin from '../../assets/img/bitcoin-logo.svg';
import custom from '../../assets/img/custom.svg';
import befea from '../../assets/img/befea.svg'
import next from '../../assets/img/arrow.svg'

class RedBlockchain extends Component {
  constructor(props) {
    super(props);
    this.state = {
      blockchain_title: 'BFA',
    };
    this.handleChange = this.handleChange.bind(this);
    this.customChange = this.customChange.bind(this);
  }

  handleChange = event => {
    this.setState({
      blockchain_title: event.target.value
    });
  };

  customChange = () => {
    this.setState({blockchain_title: 'Custom'})
  }
   
  prevStep = () => {
    // this.props.history.push('/recipients');
    return this.props.handleComponentOption('editor');    
  }

  nextStep = () => {
    // this.props.history.push('/recipients');
    return this.props.handleComponentOption('table');    
  }

  render() {
    return (
      <div>
        <div className="blockchain-choose">
          <div className="right-container" id="general-container">
            <div className="instructions-choose">
              <p>Elige la Blockchain en la que quieres que se escribirán tus certificados</p>
            </div>
            <div className="radio-container">
              <div className="flex-container">
                <FormControl component="fieldset">
                    <div className="form-element">
                      <Radio
                        checked={ this.state.blockchain_title === 'BFA' }
                        onChange={ this.handleChange }
                        value="BFA"
                        name="radio-button-demo"
                        aria-label="BFA"
                        className="radio-image"
                      />
                        <img src={ befea } alt="" width="180px"/>
                    </div>
                    <div className="form-element">
                      <Radio
                        checked={ this.state.blockchain_title === 'Bitcoin' }
                        onChange={ this.handleChange }
                        value="Bitcoin"
                        name="radio-button-demo"
                        aria-label="Bitcoin"
                        className="radio-image"
                      />
                        <img src={ Bitcoin } alt="" width="180px"/>
                    </div>
                    <div className="form-element">
                      <Radio
                        checked={ this.state.blockchain_title === 'Ethereum' }
                        onChange={ this.handleChange }
                        value="Ethereum"
                        name="radio-button-demo"
                        aria-label="Ethereum"
                        className="radio-image"
                      />
                        <img src={ Ethereum } alt="" width="180px"/>
                    </div>
                    <TextField className="personalizaed"
                      label="Personalizado"
                      variant="outlined"
                      onClick= { this.customChange }
                      id="outlined-input"/>

                </FormControl>
                <p className="info-text">* Escribe tu red para verificar si está conectada con nuestros servicios</p>
              </div>
            </div>
          </div>
          <div className="left-container">
            <div className="red-container">
              <figure>
                { this.state.blockchain_title === 'BFA' ?
                  ( <img src={ befea } className="befea-logo"/> ) : null
                }
                { this.state.blockchain_title === 'Ethereum' ?
                  ( <img src={ Ethereum } className="blockchain-logo"/> ) : null
                }
                { this.state.blockchain_title === 'Bitcoin' ?
                  ( <img src={ Bitcoin } className="blockchain-logo"/> ) : null
                }
                { this.state.blockchain_title === 'Custom' ?
                  ( <img src={ custom } className="blockchain-logo"/> ) : null
                }
              </figure>
              <div className="description-red">
                { this.state.blockchain_title === 'BFA' ?
                  ( <div className="description-befea"> <p> Ventajas en ésta red: </p>
                      <ul>
                        <li>Blockchain Federal Argentina es una plataforma multiservicios abierta y participativa pensada para integrar servicios y aplicaciones sobre blockchain</li>
                        <li>Una iniciativa confiable y completamente auditable que permita optimizar procesos y funcione como herramienta de empoderamiento para toda la comunidad.</li>
                        <li>Diseñada para potenciarse a través de los aportes de sectores públicos, privados, académicos y de la sociedad civil, BFA opta por una estrategia donde la participación de toda la comunidad es esencial</li>
                      </ul>
                    </div> ) : null
                }
                { this.state.blockchain_title === 'Ethereum' ?
                  ( <div className="description-text"> <p> Ventajas en ésta red: </p>
                      <ul>
                        <li>Es la red blockchain pionera que se enfocó en ejecución de contratos inteligentes </li>
                        <li>El tamaño de su bloque es menor a 1Mb</li>
                        <li>El calculo de la dificultad de minado se hace por cada bloque minado</li>
                      </ul>
                    </div> ) : null
                }
                { this.state.blockchain_title === 'Bitcoin' ?
                  ( <div className="description-text"> <p> Ventajas en ésta red: </p>
                    <ul>
                      <li>Es la red blockchain más famosa y con más peers en el mundo </li>
                      <li>El coste de las transacciones es el mismo para cada una de ellas</li>
                      <li>El uso de la criptomoneda sirve para pagos, competir con las divisas fiat y el oro, y también como inversión</li>
                    </ul>
                  </div> ) : null
                }
                { this.state.blockchain_title === 'Custom' ?
                  ( <div className="description-text"> <p> Ventajas en ésta red: </p>
                    <ul>
                      <li>Elige ésta red con base en tus conocimientos de ella </li>
                      <li>Las características de almacenaje de información de ésta red serán las predefinidas en su paper</li>
                    </ul>
                  </div> ) : null
                }
              </div>
            </div>
              <div className="button-container-chain">
                <div className="prev-button-chain" onClick={ this.prevStep.bind(this) }>
                  <NavigateBeforeIcon />
                  Atrás
                </div>
                <button onClick={ this.nextStep.bind(this) } className="next-button-chain" type="submit">Siguiente <img className=" icon-next" src={next} alt="next-button"/></button>
              </div>
          </div>
        </div>
      </div> 
    );
  }
}

export default RedBlockchain;