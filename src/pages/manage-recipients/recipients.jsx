import React, { Component } from 'react';
import { Spreadsheet } from "dhx-spreadsheet";
import "dhx-spreadsheet/codebase/spreadsheet.css";
import NavigateBeforeIcon from '@material-ui/icons/NavigateBefore';
import next from '../../assets/img/arrow.svg'

import './recipients.scss';

class ManageRecipients extends Component {
  componentDidMount() {
    this.spreadsheet = new Spreadsheet(this.el, {
      editLine: true,
      menu: true,
      toolbarBlocks: [
        "undo", "colors", "decoration", "align", "lock", "clear", 
        "rows", "columns", "help", "format", "file"
      ],
      autoFormat: true
    });
    if(localStorage.getItem('recipientState'))
    {
      let savedRecipient = JSON.parse(localStorage.getItem('recipientState'));
      this.spreadsheet.parse(savedRecipient); 
    }
  }
  componentWillUnmount() {
    this.spreadsheet.destructor();
  }
 
  prevStep = () => {
    // this.props.history.push('/recipients');
    return this.props.handleComponentOption('chain');
  }
  nextStep = () => {
    // this.props.history.push('/recipients');
    let state = this.spreadsheet.serialize();
    localStorage.setItem('recipientState', JSON.stringify(state));
    this.spreadsheet.parse(localStorage.getItem('recipientState'));
    return this.props.handleComponentOption('graph');  
  }

  render () {
    return (
      <div>
        <div className="textos-table">
          <h1 className="margin-none-table">Tabla de Recipientes</h1>
          <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
        </div>
        <div className="large">
          <div className="prev-button" onClick={ this.prevStep.bind(this) }>
            <NavigateBeforeIcon />
            Atrás
          </div>
          <button onClick={ this.nextStep.bind(this) } className="next-button" type="submit">Certificar<img className=" icon-next" src={next} alt="netx-button"/></button>
        </div>
        <div ref={el => this.el = el} className="widget-box" style={{ paddingTop: 25, height: 700 }}></div>
      </div>
    );
  }
}

export default ManageRecipients;
