import React, { Component } from 'react';
import NavigateBeforeIcon from '@material-ui/icons/NavigateBefore';
import EmailEditor from 'react-email-editor';
import { htmTemporal } from './template-tmp';
import visualiza from '../../assets/img/visualiza.svg';
import guarda from '../../assets/img/guardar.svg';
import './template.scss';

class Template extends Component {
  constructor(props) {
    super(props);
    this.state = {
      json: ''
    };
    this.editor = null;
    this.isEditorLoaded = false;
    this.isComponentMounted = false;
  }

  componentWillMount = () => {
    if (this.props.stateCard) {
      localStorage.setItem('saveDesign', JSON.stringify(htmTemporal));
      const htmlTemplate = localStorage.getItem('saveDesign');
      this.setState({
        json: htmlTemplate
      });
    } else {
      localStorage.removeItem('saveDesign');
    }
  }

  prevStep = () => {
    return this.props.handleComponentOption('cards');
  };

  saveDesign = () => {
    this.editor.saveDesign(design => {
      localStorage.setItem('saveDesign', JSON.stringify(design));
      return this.props.handleComponentOption('chain');
    });
  };

  exportHtml = () => {
    this.editor.exportHtml(data => {
      const { design, html } = data
      localStorage.setItem('saveDesign', JSON.stringify(design));
      localStorage.setItem('exportHtml', html);
      return this.props.handleComponentOption('preview');
    });
  };

  onLoad = () => {
    setTimeout(() => {
      if (this.state.json !== '') {
        this.editor.loadDesign(JSON.parse(this.state.json));
      }
    }, 800 )
  };


  render() {
    return (
    <div className="Template"> 
      <div className="textos">
        <div>
          <h1 className="margin-none">Editor de certificados</h1>
          <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
        </div>
        <div className="prev-template" onClick={ this.prevStep.bind(this) }>
          <NavigateBeforeIcon />
          Atrás
        </div>
      </div>
      <div className="view-template">
        <div className="display-flex" onClick={ this.exportHtml.bind(this) }>
          <img className="icon" src={ visualiza } alt="previsualiza-icon"/>
          <span>Previsualizar</span>
        </div>
        <div className="display-flex save-template" onClick={ this.saveDesign.bind(this) }>
          <img className="icon" src={ guarda } alt="guarda-icon"/>
          <span> Guardar y continuar</span>
        </div>
      </div>
      <div className="editor-container">
        <div className="wall"></div>
        <EmailEditor
          ref={ editor => this.editor = editor }
          onLoad={ this.onLoad }
        />
      </div>
    </div>
    );
  }
}

export default Template;