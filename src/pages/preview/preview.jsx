import React from 'react';
import NavigateBeforeIcon from '@material-ui/icons/NavigateBefore';

import './preview.scss';

class Preview extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      exportHtml: localStorage.getItem('exportHtml')
    };
  }

  prevStep = () => {
    // this.props.history.push('/create-template');
    return this.props.handleComponentOption('editor');
  };
 
  render () {
    return (
      <div className="template-html">
        <div className="return-template">
          <div className="textos-preview">
            <h1 className="margin-none-table">Previsualiza tu Template</h1>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
          </div>
          <div className="large-preview">
            <div className="prev-button" onClick={ this.prevStep.bind(this) }>
              <NavigateBeforeIcon />
              Atrás
            </div>
          </div>
        </div>
        <div class="preview-template" dangerouslySetInnerHTML={{ __html: this.state.exportHtml }}></div>
      </div>
    );
  }
}

export default Preview;
